from lxml import html
from httplib2 import Http
import hashlib
def do_get(url): return Http().request(url,headers={'Cookie':'PHPSESSID=8d8rvfvu1nadfiav41bfi6k6d4;'})[1] #replace session ID from your browser
challenge_url = "http://ringzer0team.com/challenges/13"
message = html.fromstring(do_get(challenge_url)).xpath('//div[@class="message"]/text()')[1].strip()
param = hashlib.sha512(message).hexdigest()
flag_page = do_get( "%s/%s" % (challenge_url,param) )
print html.fromstring(flag_page).xpath('//div[@class="alert alert-info"]/text()')[0]